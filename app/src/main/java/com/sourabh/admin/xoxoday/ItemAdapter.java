package com.sourabh.admin.xoxoday;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sourabh on 28-06-2018.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder>{


        private Context context;
        public ArrayList<ItemData> data;
        public Map<String, ItemData> selectionMap;


    public ItemAdapter(Context context) {
            this.context = context;
            data = new ArrayList<>();
        selectionMap = new HashMap<>();

    }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.list_adapter_layout, null);
            ItemViewHolder holder = new ItemViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final ItemViewHolder holder, final int position) {
            holder.offersText.setText(data.get(position).getName());

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectionMap.containsKey(data.get(position).getName())) {
                        holder.checkBox.setChecked(false);
                        selectionMap.remove(data.get(position).getName());
                    } else {
                        holder.checkBox.setChecked(true);
                        selectionMap.put(data.get(position).getName(), data.get(position));
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return (data == null ? 0 : data.size());
        }

        public static class ItemViewHolder extends RecyclerView.ViewHolder {

            TextView offersText;
            CheckBox checkBox;

            public ItemViewHolder(View itemView) {
                super(itemView);
                offersText = (TextView) itemView.findViewById(R.id.name);
                checkBox = (CheckBox) itemView.findViewById(R.id.selectionCheckBox);
            }
        }
}
